﻿using ManagementSystem.Common;
using ManagementSystem.DBContext;
using ManagementSystem.DBContext.Service;
using ManagementSystem.Models.Params;
using ManagementSystem.Models.Results;
using ManagementSystem.Models.Sys;
using ManagementSystem.WebApi.Ctrl.SysServiceCtrl;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManagementSystem.WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PermissionManageController : ControllerBase
    {

        private ISysCtrl sysCtrl;
        public PermissionManageController(ISysCtrl _sysCtrl)
        {
            sysCtrl = _sysCtrl;
        }



        #region 方法
        #region  获取Jqgrid数据
        [HttpGet]
        [Route("GetInfoListForJqgrid")]
        public ActionResult GetInfoListForJqgrid([FromQuery] PermissionInfoParam param)
        {
            int totalCount = 0;

            List<Sys_Permission> dataList = sysCtrl.GetPermissionInfoListForJqgrid(param, ref totalCount);
            List<PermissionInfoListForJqgridResult> getInfoListForJqgridResult = dataList
                .Select(o => new PermissionInfoListForJqgridResult()
                {
                    Id = o.Id,
                    Description = o.Description,
                    PermissionName = o.PermissionName,
                }).ToList();

            JqGridData jqgResult = JqGridData.ConvertIListToJqGridData(param.page, param.rows, totalCount, getInfoListForJqgridResult);
            return Ok(jqgResult);
        }
        #endregion

        #region 通过Id获取数据
        [HttpGet]
        [Route("GetDataById")]
        public ActionResult GetDataById(int Id)
        {
            ResultMessage resultMessage = new ResultMessage();
            try
            {
                ISysCtrl sysCtrl = new SysCtrl();
                Sys_Permission sys_Permission = sysCtrl.GetPermissionDataById(Id);
                resultMessage.Data = sys_Permission;
            }
            catch (Exception ex)
            {
                resultMessage.IsSuccess = false;
                resultMessage.ErrorMessage = ex.Message;
            }

            return Ok(resultMessage);
        }
        #endregion


        #region  添加数据
        [HttpPost]
        [Route("PostInsertData")]
        public ActionResult PostInsertData([FromHeader] PermissionInfoParam param)
        {
            ResultMessage resultMessage = new ResultMessage();
            try
            {
                ISysCtrl sysCtrl = new SysCtrl();
                sysCtrl.InsertPermissionData(param.PermissionName, param.Description);
            }
            catch (Exception ex)
            {
                resultMessage.IsSuccess = false;
                resultMessage.ErrorMessage = ex.Message;
            }

            return Ok(resultMessage);
        }
        #endregion

        #region  修改数据
        [HttpPost]
        [Route("PostUpdateData")]
        public ActionResult PostUpdateData([FromHeader] PermissionInfoParam param)
        {
            ResultMessage resultMessage = new ResultMessage();
            try
            {
                ISysCtrl sysCtrl = new SysCtrl();
                sysCtrl.UpdatePermissionData(param);
            }
            catch (Exception ex)
            {
                resultMessage.IsSuccess = false;
                resultMessage.ErrorMessage = ex.Message;
            }

            return Ok(resultMessage);
        }
        #endregion

        #region 删除数据通过ID集合
        [HttpPost]
        [Route("PostDeleteDataByIdList")]
        public ActionResult PostDeleteDataByIdList([FromForm] List<int> idList)
        {
            ResultMessage resultMessage = new ResultMessage();
            try
            {
                ISysCtrl sysCtrl = new SysCtrl();
                sysCtrl.PostDeletePermissionDataByIdList(idList);
            }
            catch (Exception ex)
            {
                resultMessage.IsSuccess = false;
                resultMessage.ErrorMessage = ex.Message;
            }

            return Ok(resultMessage);
        }
        #endregion
        #endregion
    }
}

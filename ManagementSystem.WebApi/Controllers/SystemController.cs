﻿using ManagementSystem.Models.Results;
using ManagementSystem.Models.Sys;
using ManagementSystem.WebApi.Common.JWT;
using ManagementSystem.WebApi.Ctrl.SysServiceCtrl;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManagementSystem.WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SystemController : ControllerBase
    {
        private ISysCtrl sysCtrl;
        private IJWTService ijwtService;
        public SystemController(ISysCtrl _sysCtrl, IJWTService _ijwtService)
        {
            sysCtrl = _sysCtrl;
            ijwtService = _ijwtService;
        }

        [HttpGet]
        [Route("GetMenuList")]
        public ActionResult GetMenuList()
        {
            ResultMessage resultMessage = new ResultMessage();

            try
            {
                List<Sys_Page> sys_PageList = sysCtrl.GetPageListByAccount("admin");
                sys_PageList = sys_PageList.OrderBy(p => p.Sequence).OrderBy(p => p.ParentId).ToList();

                resultMessage.Data = sys_PageList;
            }
            catch (Exception ex)
            {
                resultMessage.IsSuccess = false;
                resultMessage.ErrorMessage = ex.Message;
            }

            return Ok(resultMessage);
        }

        [HttpGet]
        [Route("GetToken")]
        public ActionResult GetToken(string userName= "admin")
        {
            ResultMessage resultMessage = new ResultMessage();

            try
            {
                string token = ijwtService.GetToken(userName);
                resultMessage.Data = token;
            }
            catch (Exception ex)
            {
                resultMessage.IsSuccess = false;
                resultMessage.ErrorMessage = ex.Message;
            }

            return Ok(resultMessage);
        }

    }
}

﻿using ManagementSystem.Models.Params;
using ManagementSystem.Models.Results;
using ManagementSystem.Models.Sys;
using ManagementSystem.WebApi.Ctrl.SysServiceCtrl;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManagementSystem.WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PageManageController : ControllerBase
    {
        private ISysCtrl sysCtrl;
        public PageManageController(ISysCtrl _sysCtrl)
        {
            sysCtrl = _sysCtrl;
        }

        #region 方法
        #region  获取Jqgrid数据
        [HttpGet]
        [Route("GetInfoListForJqgrid")]
        public ActionResult GetInfoListForJqgrid([FromQuery] PageInfoParam param)
        {
            int totalCount = 0;

            List<Sys_Page> dataList = sysCtrl.GetPageInfoListForJqgrid(param, ref totalCount);
            List<PageInfoListForJqgridResult> getInfoListForJqgridResult = dataList
                .Select(o => new PageInfoListForJqgridResult()
                {
                    Id = o.Id,
                    Icon = o.Icon,
                    Level = o.Level,
                    PageName = o.PageName,
                    ParentId = o.ParentId,
                    Sequence = o.Sequence,
                    Url = o.Url,
                }).ToList();

            JqGridData jqgResult = JqGridData.ConvertIListToJqGridData(param.page, param.rows, totalCount, getInfoListForJqgridResult);
            return Ok(jqgResult);
        }
        #endregion

        #region 通过Id获取数据
        [HttpGet]
        [Route("GetDataById")]
        public ActionResult GetDataById(int Id)
        {
            ResultMessage resultMessage = new ResultMessage();
            try
            {
                Sys_Page sys_Page = sysCtrl.GetPageDataById(Id);
                resultMessage.Data = sys_Page;
            }
            catch (Exception ex)
            {
                resultMessage.IsSuccess = false;
                resultMessage.ErrorMessage = ex.Message;
            }

            return Ok(resultMessage);
        }
        #endregion


        #region  添加数据
        [HttpPost]
        [Route("PostInsertData")]
        public ActionResult PostInsertData([FromHeader] PageInfoParam param)
        {
            ResultMessage resultMessage = new ResultMessage();
            try
            {
                sysCtrl.InsertPageData(param);
            }
            catch (Exception ex)
            {
                resultMessage.IsSuccess = false;
                resultMessage.ErrorMessage = ex.Message;
            }

            return Ok(resultMessage);
        }
        #endregion

        #region  添加数据
        [HttpPost]
        [Route("PostUpdateData")]
        public ActionResult PostUpdateData([FromHeader] PageInfoParam param)
        {
            ResultMessage resultMessage = new ResultMessage();
            try
            {
                sysCtrl.UpdatePageData(param);
            }
            catch (Exception ex)
            {
                resultMessage.IsSuccess = false;
                resultMessage.ErrorMessage = ex.Message;
            }

            return Ok(resultMessage);
        }
        #endregion

        #region 删除数据通过ID集合
        [HttpPost]
        [Route("PostDeleteDataByIdList")]
        public ActionResult PostDeleteDataByIdList([FromForm] List<int> idList)
        {
            ResultMessage resultMessage = new ResultMessage();
            try
            {
                sysCtrl.PostDeletePageDataByIdList(idList);
            }
            catch (Exception ex)
            {
                resultMessage.IsSuccess = false;
                resultMessage.ErrorMessage = ex.Message;
            }

            return Ok(resultMessage);
        }
        #endregion
        #endregion
    }
}

﻿using ManagementSystem.Models.Params;
using ManagementSystem.Models.Results;
using ManagementSystem.Models.Sys;
using ManagementSystem.WebApi.Ctrl.SysServiceCtrl;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManagementSystem.WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AccountManageController : ControllerBase
    {
        private ISysCtrl sysCtrl;
        public AccountManageController(ISysCtrl _sysCtrl)
        {
            sysCtrl = _sysCtrl;
        }


        #region  获取Jqgrid数据
        [HttpGet]
        [Route("GetInfoListForJqgrid")]
        public ActionResult GetInfoListForJqgrid([FromQuery] AccoutInfoParam param)
        {
            int totalCount = 0;

            List<Sys_Account> dataList = sysCtrl.GetAccountInfoListForJqgrid(param, ref totalCount);
            List<AccoutInfoListForJqgridResult> getInfoListForJqgridResult = dataList
                .Select(o => new AccoutInfoListForJqgridResult()
                {
                    Id = o.Id,
                    Account = o.Account,
                    EMail = o.EMail,
                    MobilePhone = o.MobilePhone,
                    Password = o.Password,
                }).ToList();
            JqGridData jqgResult = JqGridData.ConvertIListToJqGridData(param.page, param.rows, totalCount, getInfoListForJqgridResult);
            return Ok(jqgResult);
        }
        #endregion

        #region 通过Id获取数据
        [HttpGet]
        [Route("GetDataById")]
        public ActionResult GetDataById(int Id)
        {
            ResultMessage resultMessage = new ResultMessage();
            try
            {
                Sys_Account sys_Account = sysCtrl.GetAccountDataById(Id);
                resultMessage.Data = sys_Account;
            }
            catch (Exception ex)
            {
                resultMessage.IsSuccess = false;
                resultMessage.ErrorMessage = ex.Message;
            }

            return Ok(resultMessage);
        }
        #endregion


        #region  添加数据
        [HttpPost]
        [Route("PostInsertData")]
        public ActionResult PostInsertData([FromHeader] AccoutInfoParam param)
        {
            ResultMessage resultMessage = new ResultMessage();
            try
            {
                sysCtrl.InsertAccountData(param);
            }
            catch (Exception ex)
            {
                resultMessage.IsSuccess = false;
                resultMessage.ErrorMessage = ex.Message;
            }

            return Ok(resultMessage);
        }
        #endregion

        #region  添加数据
        [HttpPost]
        [Route("PostUpdateData")]
        public ActionResult PostUpdateData([FromHeader] AccoutInfoParam param)
        {
            ResultMessage resultMessage = new ResultMessage();
            try
            {
                sysCtrl.UpdateAccountData(param);
            }
            catch (Exception ex)
            {
                resultMessage.IsSuccess = false;
                resultMessage.ErrorMessage = ex.Message;
            }

            return Ok(resultMessage);
        }
        #endregion

        #region 删除数据通过ID集合
        [HttpPost]
        [Route("PostDeleteDataByIdList")]
        public ActionResult PostDeleteDataByIdList([FromForm] List<int> idList)
        {
            ResultMessage resultMessage = new ResultMessage();
            try
            {
                sysCtrl.PostDeleteAccountDataByIdList(idList);
            }
            catch (Exception ex)
            {
                resultMessage.IsSuccess = false;
                resultMessage.ErrorMessage = ex.Message;
            }

            return Ok(resultMessage);
        }
        #endregion
    }
}

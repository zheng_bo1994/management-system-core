﻿using ManagementSystem.Models.Params;
using ManagementSystem.Models.Results;
using ManagementSystem.Models.Sys;
using ManagementSystem.WebApi.Ctrl.SysServiceCtrl;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManagementSystem.WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RoleManageController : ControllerBase
    {
        private ISysCtrl sysCtrl;
        public RoleManageController(ISysCtrl _sysCtrl)
        {
            sysCtrl = _sysCtrl;
        }

        #region 方法
        #region  获取Jqgrid数据
        [HttpGet]
        [Route("GetInfoListForJqgrid")]
        public ActionResult GetInfoListForJqgrid([FromQuery] RoleInfoParam param)
        {
            int totalCount = 0;

            List<Sys_Role> dataList = sysCtrl.GetRoleInfoListForJqgrid(param, ref totalCount);
            List<RoleInfoListForJqgridResult> getInfoListForJqgridResult = dataList
                .Select(o => new RoleInfoListForJqgridResult()
                {
                    Id = o.Id,
                    Description = o.Description,
                    RoleName = o.RoleName,
                    RoleType = o.RoleType,
                }).ToList();

            JqGridData jqgResult = JqGridData.ConvertIListToJqGridData(param.page, param.rows, totalCount, getInfoListForJqgridResult);
            return Ok(jqgResult);
        }
        #endregion

        #region 通过Id获取数据
        [HttpGet]
        [Route("GetDataById")]
        public ActionResult GetDataById(int Id)
        {
            ResultMessage resultMessage = new ResultMessage();
            try
            {
                ISysCtrl sysCtrl = new SysCtrl();
                Sys_Role sys_Role = sysCtrl.GetRoleDataById(Id);
                resultMessage.Data = sys_Role;
            }
            catch (Exception ex)
            {
                resultMessage.IsSuccess = false;
                resultMessage.ErrorMessage = ex.Message;
            }

            return Ok(resultMessage);
        }
        #endregion


        #region  添加数据
        [HttpPost]
        [Route("PostInsertData")]
        public ActionResult PostInsertData([FromHeader] RoleInfoParam param)
        {
            ResultMessage resultMessage = new ResultMessage();
            try
            {
                ISysCtrl sysCtrl = new SysCtrl();
                sysCtrl.InsertRoleData(param);
            }
            catch (Exception ex)
            {
                resultMessage.IsSuccess = false;
                resultMessage.ErrorMessage = ex.Message;
            }

            return Ok(resultMessage);
        }
        #endregion

        #region  添加数据
        [HttpPost]
        [Route("PostUpdateData")]
        public ActionResult PostUpdateData([FromHeader] RoleInfoParam param)
        {
            ResultMessage resultMessage = new ResultMessage();
            try
            {
                ISysCtrl sysCtrl = new SysCtrl();
                sysCtrl.UpdateRoleData(param);
            }
            catch (Exception ex)
            {
                resultMessage.IsSuccess = false;
                resultMessage.ErrorMessage = ex.Message;
            }

            return Ok(resultMessage);
        }
        #endregion

        #region 删除数据通过ID集合
        [HttpPost]
        [Route("PostDeleteDataByIdList")]
        public ActionResult PostDeleteDataByIdList([FromForm] List<int> idList)
        {
            ResultMessage resultMessage = new ResultMessage();
            try
            {
                ISysCtrl sysCtrl = new SysCtrl();
                sysCtrl.PostDeleteRoleDataByIdList(idList);
            }
            catch (Exception ex)
            {
                resultMessage.IsSuccess = false;
                resultMessage.ErrorMessage = ex.Message;
            }

            return Ok(resultMessage);
        }
        #endregion
        #endregion
    }
}

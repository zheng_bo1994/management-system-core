﻿using ManagementSystem.Models.Params;
using ManagementSystem.Models.Sys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManagementSystem.WebApi.Ctrl.SysServiceCtrl
{
    public interface ISysCtrl
    {
        #region Account
        Sys_Account GetAccountByAccount(string account);
        List<Sys_Account> GetAccountInfoListForJqgrid(AccoutInfoParam param, ref int totalCount);

        void InsertAccountData(AccoutInfoParam param);

        void UpdateAccountData(AccoutInfoParam param);
        Sys_Account GetAccountDataById(int Id);
        void PostDeleteAccountDataByIdList(List<int> idList);
        #endregion

        #region Role
        List<Sys_Role> GetRoleInfoListForJqgrid(RoleInfoParam param, ref int totalCount);

        void InsertRoleData(RoleInfoParam param);

        void UpdateRoleData(RoleInfoParam param);
        Sys_Role GetRoleDataById(int Id);
        void PostDeleteRoleDataByIdList(List<int> idList);
        #endregion

        #region Permission
        List<Sys_Permission> GetPermissionInfoListForJqgrid(PermissionInfoParam param, ref int totalCount);

        void InsertPermissionData(string permissionName,string description);

        void UpdatePermissionData(PermissionInfoParam param);
        Sys_Permission GetPermissionDataById(int Id);
        void PostDeletePermissionDataByIdList(List<int> idList);
        #endregion

        #region Page
        List<Sys_Page> GetPageInfoListForJqgrid(PageInfoParam param, ref int totalCount);

        void InsertPageData(PageInfoParam param);

        void UpdatePageData(PageInfoParam param);
        Sys_Page GetPageDataById(int Id);
        void PostDeletePageDataByIdList(List<int> idList);
        #endregion

        #region 获取菜单
        List<Sys_Page> GetPageListByAccount(string account);
        #endregion
    }
}

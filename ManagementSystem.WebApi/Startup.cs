using EntityFrameworkCore.UseRowNumberForPaging;
using ManagementSystem.DBContext;
using ManagementSystem.DBContext.Interface;
using ManagementSystem.DBContext.Service;
using ManagementSystem.Models.Sys;
using ManagementSystem.WebApi.Common.JWT;
using ManagementSystem.WebApi.Ctrl.SysServiceCtrl;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace ManagementSystem.WebApi
{
    /// <summary>
    /// dotnet ManagementSystem.WebApi.dll --urls="http://*:30854" –-ip="127.0.0.1" --port=30854
    ///http://localhost:30854/swagger/index.html
    /// </summary>
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            #region 自带
            services.AddControllers().AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.PropertyNamingPolicy = null;
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "ManagementSystem.WebApi", Version = "v1" });
            });
            #endregion


            #region  EF

            services.AddDbContext<BaseContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("DbContext"), opt => opt.UseRowNumberForPaging());
            });

            services.AddScoped(typeof(IBaseService<>), typeof(BaseService<>));
            #endregion


            #region 类依赖注入
            services.AddScoped(typeof(ISysCtrl), typeof(SysCtrl));
            services.AddScoped(typeof(IJWTService), typeof(JWTService));
            #endregion

            #region 批量注入
            //加载程序集MyApplication
            //var serviceAsm = Assembly.Load(new AssemblyName("ManagementSystem.DBContext.Service"));
            //foreach (Type serviceType in serviceAsm.GetTypes().Where(t => typeof(IBaseService<>).IsAssignableFrom(t) && !t.GetTypeInfo().IsAbstract))
            //{
            //    var interfaceTypes = serviceType.GetInterfaces();
            //    foreach (var interfaceType in interfaceTypes)
            //    {
            //        services.AddScoped(interfaceType, serviceType);
            //    }
            //}
            #endregion



            #region 支持跨域  所有的Api都支持跨域
            services.AddCors(option => option.AddPolicy("AllowCors", _build => _build.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader()));
            #endregion



            #region JWT验证
            //1.Nuget引入程序包：Microsoft.AspNetCore.Authentication.JwtBearer 
            //services.Configure<JWTTokenOptions>(Configuration.GetSection("audience"));//通过实体模型进行引入
            var validAudience = this.Configuration["audience"];
            var validIssuer = this.Configuration["issuer"];
            var securityKey = this.Configuration["SecurityKey"];
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)  //默认授权机制名称；                                      
            .AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,//是否验证Issuer
                    ValidateAudience = true,//是否验证Audience
                    ValidateLifetime = true,//是否验证失效时间
                    ValidateIssuerSigningKey = true,//是否验证SecurityKey
                    ValidAudience = validAudience,//Audience
                    ValidIssuer = validIssuer,//Issuer，这两项和前面签发jwt的设置一致  表示谁签发的Token
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(securityKey))//拿到SecurityKey
                };
            });
            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "ManagementSystem.WebApi v1"));
            }

            #region 支持跨域
            app.UseCors("AllowCors");
            #endregion

            app.UseRouting();

            #region 通过中间件来支持鉴权授权
            app.UseAuthentication();
            app.UseAuthorization();
            #endregion

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}

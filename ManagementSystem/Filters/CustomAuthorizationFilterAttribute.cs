﻿using ManagementSystem.Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagementSystem.Filters
{
    public class CustomAuthorizationFilterAttribute : Attribute, IAuthorizationFilter
    {
        //private SessionHelper sessionHelper = new SessionHelper();;
        //private CookieHelper cookieHelper;
        //public CustomAuthorizationFilterAttribute(SessionHelper _sessionHelper, CookieHelper _cookieHelper)
        //{
        //    sessionHelper = _sessionHelper;
        //    cookieHelper = _cookieHelper;
        //}


        public void OnAuthorization(AuthorizationFilterContext context)
        {
            if (context.ActionDescriptor.EndpointMetadata.Any(item => item is AllowAnonymousAttribute))
            {
                return;//匿名 不检查
            }
            if (context.Filters.Any(f => f is IAllowAnonymousFilter))
            {
                return;//匿名 不检查  
            }

            //string tokenValue = sessionHelper.GetSession("token");

            //if (string.IsNullOrEmpty(tokenValue))
            //{
            //    context.Result = new RedirectResult("~/Login/Login2");
            //}
            //else
            //{
            //    //还应该检查下权限
            //    return;
            //}
        }
    }
}

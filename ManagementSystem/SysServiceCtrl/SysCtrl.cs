﻿using EntityFrameworkCore.UseRowNumberForPaging;
using ManagementSystem.Common;
using ManagementSystem.DBContext.Context;
using ManagementSystem.DBContext.Interface;
using ManagementSystem.DBContext.Service;
using ManagementSystem.Models.Params;
using ManagementSystem.Models.Sys;
using Microsoft.AspNetCore.Authentication;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManagementSystem.SysServiceCtrl
{
    public class SysCtrl : ISysCtrl
    {
        #region  依赖注入
        private IDataBaseService baseService = null;
        private DataBaseContext databaseContext = null;
        public SysCtrl(DataBaseContext _databaseContext = null, string connection = "")
        {
            if (string.IsNullOrEmpty(connection))
            {
                connection = "Data Source=.; Database=CustomersParking; User ID=sa; Password=123456; MultipleActiveResultSets=True";
            }

            if (_databaseContext == null)
            {
                DbContextOptions<DataBaseContext> dbContextOption = new DbContextOptions<DataBaseContext>();
                DbContextOptionsBuilder<DataBaseContext> dbContextOptionBuilder = new DbContextOptionsBuilder<DataBaseContext>(dbContextOption);
                _databaseContext = new DataBaseContext(dbContextOptionBuilder.UseSqlServer(connection, i =>
                {
                    i.UseRowNumberForPaging();
                }).Options);

                databaseContext = _databaseContext;
            }

            baseService = new DataBaseService(databaseContext);
        }
        #endregion

        #region Account
        public Sys_Account GetAccountByAccount(string account)
        {
            Sys_Account sys_Account = baseService.Query<Sys_Account>(p => p.Account == account).FirstOrDefault();
            return sys_Account;
        }

        public List<Sys_Account> GetAccountInfoListForJqgrid(AccoutInfoParam param, ref int totalCount)
        {
            //PageResult<Sys_Account> accountPageResult = baseService.QueryPage<Sys_Account, int>(null, param.rows, param.page, p => p.Id, true);



            var where = PredicateBuilder.True<Sys_Account>();

            IQueryable<Sys_Account> accountQueryable = baseService.Query<Sys_Account>(null);

            if (!string.IsNullOrEmpty(param.Account))
            {
                accountQueryable = accountQueryable.Where(p => p.Account.Contains(param.Account));
            }
            if (!string.IsNullOrEmpty(param.EMail))
            {
                accountQueryable = accountQueryable.Where(p => p.EMail.Contains(param.EMail));
            }
            if (!string.IsNullOrEmpty(param.MobilePhone))
            {
                accountQueryable = accountQueryable.Where(p => p.MobilePhone.Contains(param.MobilePhone));
            }


            totalCount = accountQueryable.Count();
            int indexBegin = (param.page - 1) * param.rows;
            accountQueryable = accountQueryable.OrderBy(p => p.Id).Skip(indexBegin).Take(param.rows);
            return accountQueryable.ToList();



        }

        public void InsertAccountData(AccoutInfoParam param)
        {
            Sys_Account sys_Account = new Sys_Account()
            {
                Account = param.Account,
                EMail = param.EMail,
                MobilePhone = param.MobilePhone,
                Password = param.Password,
            };
            baseService.Insert<Sys_Account>(sys_Account);
            baseService.Commit();
        }

        public void UpdateAccountData(AccoutInfoParam param)
        {
            Sys_Account sys_Account = baseService.Find<Sys_Account>(param.Id);
            sys_Account.Account = param.Account;
            sys_Account.EMail = param.EMail;
            sys_Account.MobilePhone = param.MobilePhone;
            sys_Account.Password = param.Password;

            baseService.Update<Sys_Account>(sys_Account);
            baseService.Commit();
        }
        public Sys_Account GetAccountDataById(int Id)
        {
            return baseService.Find<Sys_Account>(Id);
        }

        public void PostDeleteAccountDataByIdList(List<int> idList)
        {
            List<Sys_Account> sys_AccountList = baseService.Query<Sys_Account>(p => idList.Contains(p.Id)).ToList();

            baseService.Delete<Sys_Account>(sys_AccountList);
            baseService.Commit();
        }
        #endregion

        #region Role
        public List<Sys_Role> GetRoleInfoListForJqgrid(RoleInfoParam param, ref int totalCount)
        {
            var where = PredicateBuilder.True<Sys_Role>();

            IQueryable<Sys_Role> roleQueryable = baseService.Query<Sys_Role>(where);

            if (!string.IsNullOrEmpty(param.RoleName))
            {
                roleQueryable = roleQueryable.Where(p => p.RoleName.Contains(param.RoleName));
            }
            if (!string.IsNullOrEmpty(param.RoleType))
            {
                roleQueryable = roleQueryable.Where(p => p.RoleType.Contains(param.RoleType));
            }
            if (!string.IsNullOrEmpty(param.Description))
            {
                roleQueryable = roleQueryable.Where(p => p.Description.Contains(param.Description));
            }


            totalCount = roleQueryable.Count();
            int indexBegin = (param.page - 1) * param.rows;
            roleQueryable = roleQueryable.OrderBy(p => p.Id).Skip(indexBegin).Take(param.rows);
            return roleQueryable.ToList();
        }

        public void InsertRoleData(RoleInfoParam param)
        {
            Sys_Role sys_Role = new Sys_Role()
            {
                Description = param.Description,
                RoleName = param.RoleName,
                RoleType = param.RoleType,
            };
            baseService.Insert<Sys_Role>(sys_Role);
            baseService.Commit();
        }

        public void UpdateRoleData(RoleInfoParam param)
        {
            Sys_Role sys_Role = baseService.Find<Sys_Role>(param.Id);
            sys_Role.Description = param.Description;
            sys_Role.RoleName = param.RoleName;
            sys_Role.RoleType = param.RoleType;

            baseService.Update<Sys_Role>(sys_Role);
            baseService.Commit();
        }
        public Sys_Role GetRoleDataById(int Id)
        {
            return baseService.Find<Sys_Role>(Id);
        }

        public void PostDeleteRoleDataByIdList(List<int> idList)
        {
            List<Sys_Role> sys_RoleList = baseService.Query<Sys_Role>(p => idList.Contains(p.Id)).ToList();

            baseService.Delete<Sys_Role>(sys_RoleList);
            baseService.Commit();
        }
        #endregion

        #region Permission
        public List<Sys_Permission> GetPermissionInfoListForJqgrid(PermissionInfoParam param, ref int totalCount)
        {

            IQueryable<Sys_Permission> permissionQueryable = baseService.Set<Sys_Permission>();

            if (!string.IsNullOrEmpty(param.PermissionName))
            {
                permissionQueryable = permissionQueryable.Where(p => p.PermissionName.Contains(param.PermissionName));
            }
            if (!string.IsNullOrEmpty(param.Description))
            {
                permissionQueryable = permissionQueryable.Where(p => p.Description.Contains(param.Description));
            }


            totalCount = permissionQueryable.Count();
            int indexBegin = (param.page - 1) * param.rows;
            permissionQueryable = permissionQueryable.OrderBy(p => p.Id).Skip(indexBegin).Take(param.rows);
            return permissionQueryable.ToList();
        }

        public void InsertPermissionData(PermissionInfoParam param)
        {
            Sys_Permission sys_Permission = new Sys_Permission()
            {
                Description = param.Description,
                PermissionName = param.PermissionName,
            };
            baseService.Insert<Sys_Permission>(sys_Permission);
            baseService.Commit();
        }

        public void UpdatePermissionData(PermissionInfoParam param)
        {
            Sys_Permission sys_Permission = baseService.Find<Sys_Permission>(param.Id);
            sys_Permission.Description = param.Description;
            sys_Permission.PermissionName = param.PermissionName;

            baseService.Update<Sys_Permission>(sys_Permission);
            baseService.Commit();
        }
        public Sys_Permission GetPermissionDataById(int Id)
        {
            return baseService.Find<Sys_Permission>(Id);
        }

        public void PostDeletePermissionDataByIdList(List<int> idList)
        {
            List<Sys_Permission> sys_PermissionList = baseService.Query<Sys_Permission>(p => idList.Contains(p.Id)).ToList();

            baseService.Delete<Sys_Permission>(sys_PermissionList);
            baseService.Commit();
        }
        #endregion

        #region Page
        public List<Sys_Page> GetPageInfoListForJqgrid(PageInfoParam param, ref int totalCount)
        {
            IQueryable<Sys_Page> pageQueryable = baseService.Set<Sys_Page>();
            var pageList = baseService.Set<Sys_Page>().ToList();

            if (!string.IsNullOrEmpty(param.Icon))
            {
                pageQueryable = pageQueryable.Where(p => p.Icon.Contains(param.Icon));
            }
            if (!string.IsNullOrEmpty(param.PageName))
            {
                pageQueryable = pageQueryable.Where(p => p.PageName.Contains(param.PageName));
            }
            if (!string.IsNullOrEmpty(param.Url))
            {
                pageQueryable = pageQueryable.Where(p => p.Url.Contains(param.Url));
            }


            totalCount = pageQueryable.Count();
            int indexBegin = (param.page - 1) * param.rows;
            pageQueryable = pageQueryable.OrderBy(p => p.Id).Skip(indexBegin).Take(param.rows);
            return pageQueryable.ToList();
        }

        public void InsertPageData(PageInfoParam param)
        {
            Sys_Page sys_Page = new Sys_Page()
            {
                Icon = param.Icon,
                Level = param.Level,
                PageName = param.PageName,
                ParentId = param.ParentId,
                Sequence = param.Sequence,
                Url = param.Url,
            };
            baseService.Insert<Sys_Page>(sys_Page);
            baseService.Commit();
        }

        public void UpdatePageData(PageInfoParam param)
        {
            Sys_Page sys_Page = baseService.Find<Sys_Page>(param.Id);
            sys_Page.Icon = param.Icon;
            sys_Page.Level = param.Level;
            sys_Page.PageName = param.PageName;
            sys_Page.ParentId = param.ParentId;
            sys_Page.Sequence = param.Sequence;
            sys_Page.Url = param.Url;

            baseService.Update<Sys_Page>(sys_Page);
            baseService.Commit();
        }
        public Sys_Page GetPageDataById(int Id)
        {
            return baseService.Find<Sys_Page>(Id);
        }

        public void PostDeletePageDataByIdList(List<int> idList)
        {
            List<Sys_Page> sys_PageList = baseService.Query<Sys_Page>(p => idList.Contains(p.Id)).ToList();

            baseService.Delete<Sys_Page>(sys_PageList);
            baseService.Commit();
        }
        #endregion

        #region 获取菜单
        public List<Sys_Page> GetPageListByAccount(string account)
        {
            List<Sys_Page> sys_PageList = new List<Sys_Page>();

            IQueryable<Sys_Account> accountQueryable = this.baseService.Query<Sys_Account>(p => p.Account == account);
            List<int> accountIdList = accountQueryable.Select(p => p.Id).Distinct().ToList();
            IQueryable<Sys_AccountRelRole> accountRelRoleQueryable = this.baseService.Query<Sys_AccountRelRole>(p => accountIdList.Contains(p.AccountId));

            List<int> roleIdList = accountRelRoleQueryable.Select(p => p.RoleId).Distinct().ToList();
            IQueryable<Sys_Role> roleQueryable = this.baseService.Query<Sys_Role>(p => roleIdList.Contains(p.Id));
            IQueryable<Sys_RoleRelPermission> roleRelPermissionQueryable = this.baseService.Query<Sys_RoleRelPermission>(p => roleIdList.Contains(p.RoleId));

            List<int> permissionIdList = roleRelPermissionQueryable.Select(p => p.PermissionId).Distinct().ToList();
            IQueryable<Sys_Permission> permissionQueryable = this.baseService.Query<Sys_Permission>(p => permissionIdList.Contains(p.Id));
            IQueryable<Sys_PermissionRelPage> permissionRelPageQueryable = this.baseService.Query<Sys_PermissionRelPage>(p => permissionIdList.Contains(p.PermissionId));

            //间接到界面
            List<int> pageIdIndirectList = permissionRelPageQueryable.Select(p => p.PageId).Distinct().ToList();
            IQueryable<Sys_Page> pageIndirectQueryable = this.baseService.Query<Sys_Page>(p => pageIdIndirectList.Contains(p.Id));

            //直接到界面
            IQueryable<Sys_AccountRelPage> accountRelPageQueryable = this.baseService.Query<Sys_AccountRelPage>(p => accountIdList.Contains(p.AccountId));
            List<int> pageIdDirectList = accountRelPageQueryable.Select(p => p.PageId).Distinct().ToList();
            IQueryable<Sys_Page> pageDirectQueryable = this.baseService.Query<Sys_Page>(p => pageIdDirectList.Contains(p.Id));

            sys_PageList = pageIndirectQueryable.Union(pageDirectQueryable).ToList();

            sys_PageList = sys_PageList.Where((p, i) => sys_PageList.FindIndex(q => q.Id == p.Id) == i).ToList();

            return sys_PageList;
        }
        #endregion
    }
}

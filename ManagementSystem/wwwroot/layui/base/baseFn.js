﻿(function ($) {
    //判断是否为空 
    var isNull = function (obj) {
        var result = false

        if (typeof (obj) == "undefined") {
            result = true;
        }

        if (obj == null) {
            result = true;
        }

        if (typeof (obj) == "string" && obj == "") {
            result = true;
        }

        return result;
    }

    //ajax统一请求接口
    var ajaxRequest = function (opts) {
        var options;
        options = {
            type: "post",
            async: true,
            success: function (data, status, response) {
                if (!isNull(opts.success)) {
                    return opts.success(data);
                }
            },
            error: function (data, status, response) {
                if (!isNull(opts.error)) {
                    return opts.error(data, status, response);
                }
            }
        };
        if (!isNull(opts.url)) {
            options.url = opts.url;
        }
        if (!isNull(opts.type)) {
            options.type = opts.type;
        }
        if (!isNull(opts.async)) {
            options.async = opts.async;
        }
        if (!isNull(opts.data)) {
            options.data = opts.data;
        }
        if (!isNull(opts.dataType)) {
            options.dataType = opts.dataType;
        }
        if (!isNull(opts.contentType)) {
            options.contentType = opts.contentType;
        }
        if (!isNull(opts.processData)) {
            options.processData = opts.processData;
        }
        if (!isNull(opts.crossDomain)) {
            options.crossDomain = opts.crossDomain;
        }
        return jQuery.ajax(options);
    };


    var getApiUrl = function (rawUrl) {
        return 'http://localhost:30854' + "/api" + rawUrl;
    };

    var ajaxRequestWhen = function (opts, callBackFun) {
        var ajaxresult = ajaxRequest(opts);

        jQuery.when(ajaxresult).done(function () {
            if (callBackFun != undefined && typeof callBackFun == 'function') {
                callBackFun();
            }
        })
    }

    //获取当前容器内有name属性的控件值，返回一个对象。spliter:同名控件值的分隔符
    var getJson = function ($container, spliter) {
        spliter = spliter || "||";
        var jsonNameValue = {};
        var appendJsonData = function (jdata, name, value) {
            if ("" == name || undefined == name) {
                return jdata;
            }
            if (undefined != jdata[jQuery.trim(name)]) {
                jdata[name] += spliter + jQuery.trim(value);
            } else {
                jdata[jQuery.trim(name)] = jQuery.trim(value);
            }
            return jdata;
        };

        var simpleControls = $container.find("input[type='text'],input[type='hidden'],input[type='password'],textarea,select");
        for (var i = 0; i < simpleControls.length; i++) {
            var c = jQuery(simpleControls[i]);
            var controlName = c.attr("name");
            appendJsonData(jsonNameValue, controlName, c.val());
        }

        var radioControls = $container.find("input[type='radio'],input[type='checkbox']");
        for (var i = 0; i < radioControls.length; i++) {
            var c = jQuery(radioControls[i]);
            var controlName = c.attr("name");
            if (c.is(":checked")) {
                appendJsonData(jsonNameValue, controlName, c.val());
            }
        }

        var fileControls = $container.find("input[type='file']");
        for (var i = 0; i < fileControls.length; i++) {
            var c = jQuery(fileControls[i]);
            var controlName = c.attr("name");
            if (c[0].files.length > 0) {
                if (jsonNameValue.hasOwnProperty(controlName) && !isArray(jsonNameValue[controlName])) {
                    var oldvalue = jsonNameValue[controlName];
                    jsonNameValue[controlName] = [];
                    jsonNameValue[controlName].push(oldvalue);
                }
                if (isArray(jsonNameValue[controlName])) {
                    jsonNameValue[controlName].push(c[0].files[0]);
                }
                else {
                    jsonNameValue[controlName] = c[0].files[0];
                }
            }
        }
        return jsonNameValue;
    }

    //将json数据赋给某容器内对应name属性的控件。spliter:同名控件值的分隔符
    var setJson = function ($container, jdata, spliter) {
        spliter = spliter || "||";
        for (var p in jdata) {
            jdata[p] = jQuery.trim(jdata[p]);
            var $c = $container.find("[name='" + p + "']");
            if ($c.length > 0) {
                if (false === jdata[p] || "false" == jdata[p].toLowerCase()) {
                    jdata[p] = "False";
                } else if (true === jdata[p] || "true" == jdata[p].toLowerCase()) {
                    jdata[p] = "True";
                }
                var cName = $c[0].tagName.toLowerCase() || $c[0].nodeName.toLowerCase() || $c[0].localName;
                var cType = $c[0].type.toLowerCase();
                if (("input" == cName && ("text" == cType || "hidden" == cType || "password" == cType))
                    || "textarea" == cName || "select" == cName) {
                    $c.val(jdata[p]);
                } else if ("input" == cName && "checkbox" == cType) {
                    var chkValue = jdata[p];
                    $c.prop("checked", false);
                    if (!chkValue) {
                        return false;
                    }
                    var arrChkValue = chkValue.split(spliter);
                    for (var i = 0; i < arrChkValue.length; i++) {
                        for (var k = 0; k < $c.length; k++) {
                            if ($c[k].value == arrChkValue[i]) {
                                $($c[k]).prop("checked", true);
                                break;
                            }
                        }
                    }
                } else if ("input" == cName && "radio" == cType) {
                    for (var i = 0; i < $c.length; i++) {
                        if ($c[i].value == jdata[p]) {
                            $($c[i]).prop("checked", true);
                            break;
                        }
                    }
                } else if ("div" == cName) {
                    setJson($c, jdata);
                }

                var onchangeFlag = $c.data("event-change")
                if (onchangeFlag) {
                    $c.change();
                }

            }
        }
    }
    //获取当前容器内有name属性的控件值，返回一个对象。同名对象用List接收 只能在支持HTML5的浏览器使用
    var getForm = function ($container) {
        var jsondata = getJson($container);
        var formData = new FormData();
        for (var p in jsondata) {
            var pItem = jsondata[p];
            if (isArray(pItem)) {
                for (var i = 0; i < pItem.length; i++) {
                    formData.append(p, pItem[i]);
                }
            }
            else {
                formData.append(p, pItem || "");
            }
        }
        return formData;
    }
    //清空数据
    var clearForm = function ($container, nofields) {
        var jsonData = getJson($container);
        for (var p in jsonData) {
            if (nofields.indexOf(p) == -1) {
                jsonData[p] = "";
            }
        }
        setJson($container, jsonData);
    }



    var ext = {
        isNull,
        ajaxRequest,
        ajaxRequestWhen,
        getApiUrl,
        getJson,
        setJson,
        getForm,
        clearForm,
    }

    window.ext = ext;
    window.apiUrl = 'http://localhost:30854/api'

}).call(this);


(function () {
    var initModule = function (modules, callback, checkPermession) {
        var allModules = ['index'];
        if (typeof modules != "undefined") {
            allModules = allModules.concat(modules);
        }

        layui.config({
            base: '../../layui/layuiadmin/' //静态资源所在路径
        }).extend({
            index: 'lib/index' //主入口模块
        }).use(allModules, function () {
            if (typeof callback != "function") {
                throw new Error("layui use has no callback");
            }

            callback();
            if (typeof checkPermession == "undefined") {
                checkPermession = true;
            }
            if (checkPermession) {
                // Base.initPermession();
            }
            //layui.autoHeight();
        });
    }


    var layuiDlg = {
        init: function (option) {
            var defaultOption = {
                type: 2,
                shadeClose: true,
                shade: 0.5,
                btn: ['确定', '关闭'],
            };
            option = $.extend(defaultOption, option);
            layer.open(option);
        }
    }

    

    

    layui.initModule = initModule;
    layui.showDlg = layuiDlg.init;
}).call(this);
﻿using ManagementSystem.Models.Params;
using ManagementSystem.Models.Results;
using ManagementSystem.Models.Sys;
using ManagementSystem.SysServiceCtrl;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManagementSystem.Areas.Ace.Controllers
{
    /// <summary>
    /// 账户管理控制器
    /// </summary>
    [Area("Ace")]
    public class RoleManageController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        #region 方法
        #region  获取Jqgrid数据
        public ActionResult GetInfoListForJqgrid(RoleInfoParam param)
        {
            try
            {
                int totalCount = 0;

                ISysCtrl sysCtrl = new SysCtrl();
                List<Sys_Role> dataList = sysCtrl.GetRoleInfoListForJqgrid(param, ref totalCount);
                List<RoleInfoListForJqgridResult> getInfoListForJqgridResult = dataList
                   .Select(o => new RoleInfoListForJqgridResult()
                   {
                       Id = o.Id,
                       RoleName = o.RoleName,
                       Description = o.Description,
                       RoleType = o.RoleType,
                   }).ToList();




                JqGridData jqgResult = JqGridData.ConvertIListToJqGridData(param.page, param.rows, totalCount, getInfoListForJqgridResult);
                return Json(jqgResult);
            }
            catch (Exception ex)
            {
                throw new Exception($"{ex.Message}");
            }
        }
        #endregion

        #region 通过Id获取数据
        public ActionResult GetDataById(int Id)
        {
            ResultMessage resultMessage = new ResultMessage();
            try
            {
                ISysCtrl sysCtrl = new SysCtrl();
                Sys_Role sys_Role = sysCtrl.GetRoleDataById(Id);
                resultMessage.Data = sys_Role;
            }
            catch (Exception ex)
            {
                resultMessage.IsSuccess = false;
                resultMessage.ErrorMessage = ex.Message;
            }

            return Json(resultMessage);
        }
        #endregion


        #region  添加数据
        public ActionResult PostInsertData(RoleInfoParam param)
        {
            ResultMessage resultMessage = new ResultMessage();
            try
            {
                ISysCtrl sysCtrl = new SysCtrl();
                sysCtrl.InsertRoleData(param);
            }
            catch (Exception ex)
            {
                resultMessage.IsSuccess = false;
                resultMessage.ErrorMessage = ex.Message;
            }

            return Json(resultMessage);
        }
        #endregion

        #region  添加数据
        public ActionResult PostUpdateData(RoleInfoParam param)
        {
            ResultMessage resultMessage = new ResultMessage();
            try
            {
                ISysCtrl sysCtrl = new SysCtrl();
                sysCtrl.UpdateRoleData(param);
            }
            catch (Exception ex)
            {
                resultMessage.IsSuccess = false;
                resultMessage.ErrorMessage = ex.Message;
            }

            return Json(resultMessage);
        }
        #endregion

        #region 删除数据通过ID集合
        public ActionResult PostDeleteDataByIdList(List<int> idList)
        {
            ResultMessage resultMessage = new ResultMessage();
            try
            {
                ISysCtrl sysCtrl = new SysCtrl();
                sysCtrl.PostDeleteRoleDataByIdList(idList);
            }
            catch (Exception ex)
            {
                resultMessage.IsSuccess = false;
                resultMessage.ErrorMessage = ex.Message;
            }

            return Json(resultMessage);
        }
        #endregion
        #endregion
    }
}

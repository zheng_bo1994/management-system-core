﻿using ManagementSystem.Models.Params;
using ManagementSystem.Models.Results;
using ManagementSystem.Models.Sys;
using ManagementSystem.SysServiceCtrl;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManagementSystem.Areas.Ace.Controllers
{
    /// <summary>
    /// 账户管理控制器
    /// </summary>
    [Area("Ace")]
    public class PageManageController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        #region 方法
        #region  获取Jqgrid数据
        public ActionResult GetInfoListForJqgrid(PageInfoParam param)
        {
            try
            {
                int totalCount = 0;

                ISysCtrl sysCtrl = new SysCtrl();
                List<Sys_Page> dataList = sysCtrl.GetPageInfoListForJqgrid(param, ref totalCount);
                List<PageInfoListForJqgridResult> getInfoListForJqgridResult = dataList
                    .Select(o => new PageInfoListForJqgridResult()
                    {
                        Id = o.Id,
                        Icon = o.Icon,
                        Level = o.Level,
                        PageName = o.PageName,
                        ParentId = o.ParentId,
                        Sequence = o.Sequence,
                        Url = o.Url,
                    }).ToList();



                JqGridData jqgResult = JqGridData.ConvertIListToJqGridData(param.page, param.rows, totalCount, getInfoListForJqgridResult);
                return Json(jqgResult);
            }
            catch (Exception ex)
            {
                throw new Exception($"{ex.Message}");
            }
        }
        #endregion

        #region 通过Id获取数据
        public ActionResult GetDataById(int Id)
        {
            ResultMessage resultMessage = new ResultMessage();
            try
            {
                ISysCtrl sysCtrl = new SysCtrl();
                Sys_Page sys_Page = sysCtrl.GetPageDataById(Id);
                resultMessage.Data = sys_Page;
            }
            catch (Exception ex)
            {
                resultMessage.IsSuccess = false;
                resultMessage.ErrorMessage = ex.Message;
            }

            return Json(resultMessage);
        }
        #endregion


        #region  添加数据
        public ActionResult PostInsertData(PageInfoParam param)
        {
            ResultMessage resultMessage = new ResultMessage();
            try
            {
                ISysCtrl sysCtrl = new SysCtrl();
                sysCtrl.InsertPageData(param);
            }
            catch (Exception ex)
            {
                resultMessage.IsSuccess = false;
                resultMessage.ErrorMessage = ex.Message;
            }

            return Json(resultMessage);
        }
        #endregion

        #region  添加数据
        public ActionResult PostUpdateData(PageInfoParam param)
        {
            ResultMessage resultMessage = new ResultMessage();
            try
            {
                ISysCtrl sysCtrl = new SysCtrl();
                sysCtrl.UpdatePageData(param);
            }
            catch (Exception ex)
            {
                resultMessage.IsSuccess = false;
                resultMessage.ErrorMessage = ex.Message;
            }

            return Json(resultMessage);
        }
        #endregion

        #region 删除数据通过ID集合
        public ActionResult PostDeleteDataByIdList(List<int> idList)
        {
            ResultMessage resultMessage = new ResultMessage();
            try
            {
                ISysCtrl sysCtrl = new SysCtrl();
                sysCtrl.PostDeletePageDataByIdList(idList);
            }
            catch (Exception ex)
            {
                resultMessage.IsSuccess = false;
                resultMessage.ErrorMessage = ex.Message;
            }

            return Json(resultMessage);
        }
        #endregion
        #endregion
    }
}

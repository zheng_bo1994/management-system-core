﻿using ManagementSystem.Models.Params;
using ManagementSystem.Models.Results;
using ManagementSystem.Models.Sys;
using ManagementSystem.SysServiceCtrl;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManagementSystem.Areas.LayUI.Controllers
{
    [Area("LayUI")]
    public class AccountManageController : BaseController
    {
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult AccountManage_AddOrUpdate()
        {
            return View();
        }


        #region 方法
        #region  获取Jqgrid数据
        public ActionResult GetInfoListForJqgrid(AccoutInfoParam param)
        {
            try
            {
                int totalCount = 0;

                ISysCtrl sysCtrl = new SysCtrl();
                List<Sys_Account> dataList = sysCtrl.GetAccountInfoListForJqgrid(param, ref totalCount);
                List<AccoutInfoListForJqgridResult> getInfoListForJqgridResult = dataList
                    .Select(o => new AccoutInfoListForJqgridResult()
                    {
                        Id = o.Id,
                        Account = o.Account,
                        EMail = o.EMail,
                        MobilePhone = o.MobilePhone,
                        Password = o.Password,
                    }).ToList();

                JqGridData jqgResult = JqGridData.ConvertIListToJqGridData(param.page, param.rows, totalCount, getInfoListForJqgridResult);
                return Json(jqgResult);
            }
            catch (Exception ex)
            {
                throw new Exception($"{ex.Message}");
            }
        }
        #endregion

        #region 通过Id获取数据
        public ActionResult GetDataById(int Id)
        {
            ResultMessage resultMessage = new ResultMessage();
            try
            {
                ISysCtrl sysCtrl = new SysCtrl();
                Sys_Account sys_Account = sysCtrl.GetAccountDataById(Id);
                resultMessage.Data = sys_Account;
            }
            catch (Exception ex)
            {
                resultMessage.IsSuccess = false;
                resultMessage.ErrorMessage = ex.Message;
            }

            return Json(resultMessage);
        }
        #endregion


        #region  添加数据
        public ActionResult PostInsertData(AccoutInfoParam param)
        {
            ResultMessage resultMessage = new ResultMessage();
            try
            {
                ISysCtrl sysCtrl = new SysCtrl();
                sysCtrl.InsertAccountData(param);
            }
            catch (Exception ex)
            {
                resultMessage.IsSuccess = false;
                resultMessage.ErrorMessage = ex.Message;
            }

            return Json(resultMessage);
        }
        #endregion

        #region  添加数据
        public ActionResult PostUpdateData(AccoutInfoParam param)
        {
            ResultMessage resultMessage = new ResultMessage();
            try
            {
                ISysCtrl sysCtrl = new SysCtrl();
                sysCtrl.UpdateAccountData(param);
            }
            catch (Exception ex)
            {
                resultMessage.IsSuccess = false;
                resultMessage.ErrorMessage = ex.Message;
            }

            return Json(resultMessage);
        }
        #endregion

        #region 删除数据通过ID集合
        public ActionResult PostDeleteDataByIdList(List<int> idList)
        {
            ResultMessage resultMessage = new ResultMessage();
            try
            {
                ISysCtrl sysCtrl = new SysCtrl();
                sysCtrl.PostDeleteAccountDataByIdList(idList);
            }
            catch (Exception ex)
            {
                resultMessage.IsSuccess = false;
                resultMessage.ErrorMessage = ex.Message;
            }

            return Json(resultMessage);
        }
        #endregion
        #endregion
    }
}

﻿using ManagementSystem.Models.Params;
using ManagementSystem.Models.Results;
using ManagementSystem.Models.Sys;
using ManagementSystem.SysServiceCtrl;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManagementSystem.Areas.LayUI.Controllers
{
    [Area("LayUI")]
    public class PermissionManageController : BaseController
    {
        public IActionResult Index()
        {
            return View();
        }


        public IActionResult PermissionManage_AddOrUpdate()
        {
            return View();
        }



        #region 方法
        #region  获取Jqgrid数据
        public ActionResult GetInfoListForJqgrid(PermissionInfoParam param)
        {
            try
            {
                int totalCount = 0;

                ISysCtrl sysCtrl = new SysCtrl();
                List<Sys_Permission> dataList = sysCtrl.GetPermissionInfoListForJqgrid(param, ref totalCount);
                List<PermissionInfoListForJqgridResult> getInfoListForJqgridResult = dataList
                    .Select(o => new PermissionInfoListForJqgridResult()
                    {
                        Id = o.Id,
                        Description = o.Description,
                        PermissionName = o.PermissionName,
                    }).ToList();

                JqGridData jqgResult = JqGridData.ConvertIListToJqGridData(param.page, param.rows, totalCount, getInfoListForJqgridResult);
                return Json(jqgResult);
            }
            catch (Exception ex)
            {
                throw new Exception($"{ex.Message}");
            }
        }
        #endregion

        #region 通过Id获取数据
        public ActionResult GetDataById(int Id)
        {
            ResultMessage resultMessage = new ResultMessage();
            try
            {
                ISysCtrl sysCtrl = new SysCtrl();
                Sys_Permission sys_Permission = sysCtrl.GetPermissionDataById(Id);
                resultMessage.Data = sys_Permission;
            }
            catch (Exception ex)
            {
                resultMessage.IsSuccess = false;
                resultMessage.ErrorMessage = ex.Message;
            }

            return Json(resultMessage);
        }
        #endregion


        #region  添加数据
        public ActionResult PostInsertData(PermissionInfoParam param)
        {
            ResultMessage resultMessage = new ResultMessage();
            try
            {
                ISysCtrl sysCtrl = new SysCtrl();
                sysCtrl.InsertPermissionData(param);
            }
            catch (Exception ex)
            {
                resultMessage.IsSuccess = false;
                resultMessage.ErrorMessage = ex.Message;
            }

            return Json(resultMessage);
        }
        #endregion

        #region  添加数据
        public ActionResult PostUpdateData(PermissionInfoParam param)
        {
            ResultMessage resultMessage = new ResultMessage();
            try
            {
                ISysCtrl sysCtrl = new SysCtrl();
                sysCtrl.UpdatePermissionData(param);
            }
            catch (Exception ex)
            {
                resultMessage.IsSuccess = false;
                resultMessage.ErrorMessage = ex.Message;
            }

            return Json(resultMessage);
        }
        #endregion

        #region 删除数据通过ID集合
        public ActionResult PostDeleteDataByIdList(List<int> idList)
        {
            ResultMessage resultMessage = new ResultMessage();
            try
            {
                ISysCtrl sysCtrl = new SysCtrl();
                sysCtrl.PostDeletePermissionDataByIdList(idList);
            }
            catch (Exception ex)
            {
                resultMessage.IsSuccess = false;
                resultMessage.ErrorMessage = ex.Message;
            }

            return Json(resultMessage);
        }
        #endregion
        #endregion
    }
}

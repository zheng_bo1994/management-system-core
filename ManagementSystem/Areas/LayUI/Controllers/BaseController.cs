﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManagementSystem.Areas.LayUI.Controllers
{
    public class BaseController : Controller
    {
        public static IHttpContextAccessor _httpContextAccessor = new HttpContextAccessor();
        #region 注册Session

        public object GetSession(string key)
        {
            return _httpContextAccessor.HttpContext.Session.GetString(key);
        }

        public void SetSession(string key, string value)
        {
            _httpContextAccessor.HttpContext.Session.SetString(key, value);
        }

        #endregion

        #region 执行结果返回对象

        public class ResultMessage
        {
            public ResultMessage()
            {
                IsSuccess = true;
            }
            public bool IsSuccess { get; set; }
            public string ErrorMessage { get; set; }
            public object Data { get; set; }
        }

        #endregion

        #region 日期时间格式枚举
        protected static class EnumDateTimeFormatter
        {
            public const string Dateonly = "yyyy-MM-dd";
            public const string Dateandtime = "yyyy-MM-dd HH:mm:ss";
            public const string Timeonly = "HH:mm:ss";
        }
        #endregion
    }
}

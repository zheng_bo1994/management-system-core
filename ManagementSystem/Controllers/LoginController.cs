﻿using ManagementSystem.Common;
using ManagementSystem.WebApi.Common.JWT;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManagementSystem.Controllers
{
    public class LoginController : BaseController
    {
        private SessionHelper sessionHelper;
        private CookieHelper cookieHelper;
        public LoginController(SessionHelper _sessionHelper, CookieHelper _cookieHelper) {
            sessionHelper = _sessionHelper;
            cookieHelper = _cookieHelper;
        }


        public IActionResult Index()
        {
            return View();
        }


        public IActionResult Login()
        {
            return View();
        }
        public IActionResult Login2()
        {
            return View();
        }


        public class LoginParam
        {
            public string userName { get; set; }
            public string passWord { get; set; }
            public string frameType { get; set; }
        }
        [HttpPost]
        public IActionResult GetLoginByModel(LoginParam param)
        {
            ResultMessage resultMessage = new ResultMessage();
            try
            {
                if (param.userName.ToUpper().StartsWith("ADMIN"))
                {
                    resultMessage.Data = "请求成功";
                }
            }
            catch (Exception ex)
            {
                resultMessage.IsSuccess = false;
                resultMessage.ErrorMessage = ex.Message;
            }

            return Json(resultMessage);
        }


        [AllowAnonymousAttribute]
        public ActionResult GetAccountVerificate()
        {
            ResultMessage resultMessage = new ResultMessage();
            try
            {
                //Dictionary<string, string> headers = new Dictionary<string, string>();
                //headers.Add("userName", "admin");
                //var token = HttpHelper.HttpGet("http://localhost:30854/api/System/GetToken", headers);

                //ResultMessage oresultMessage = Newtonsoft.Json.JsonConvert.DeserializeObject<ResultMessage>(token);
                //resultMessage = oresultMessage;

                //sessionHelper.SetSession("token", oresultMessage.Data.ToString());

            }
            catch (Exception ex)
            {
                resultMessage.IsSuccess = false;
                resultMessage.ErrorMessage = ex.Message;
            }
            return Json(resultMessage);
        }
    }
}

﻿using ManagementSystem.Models.Sys;
using ManagementSystem.SysServiceCtrl;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManagementSystem.Controllers
{
    public class SystemController : BaseController
    {
        public IActionResult Index()
        {
            return View();
        }
        public ActionResult GetMenuList()
        {
            ResultMessage resultMessage = new ResultMessage();

            try
            {
                ISysCtrl sysCtrl = new SysCtrl();
                List<Sys_Page> sys_PageList = sysCtrl.GetPageListByAccount("admin");
                sys_PageList = sys_PageList.OrderBy(p => p.Sequence).OrderBy(p => p.ParentId).ToList();

                resultMessage.Data = sys_PageList;
            }
            catch (Exception ex)
            {
                resultMessage.IsSuccess = false;
                resultMessage.ErrorMessage = ex.Message;
            }

            return Json(resultMessage);
        }



    }
}

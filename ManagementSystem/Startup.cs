using EntityFrameworkCore.UseRowNumberForPaging;
using ManagementSystem.Common;
using ManagementSystem.DBContext;
using ManagementSystem.DBContext.Context;
using ManagementSystem.DBContext.Interface;
using ManagementSystem.DBContext.Service;
using ManagementSystem.Models.Order;
using ManagementSystem.WebApi.Common.JWT;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagementSystem
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().AddNewtonsoftJson(option =>
            {
                // 防止将大写转换成小写
                option.SerializerSettings.ContractResolver = new DefaultContractResolver();

                //option.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                //option.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";

            }).AddRazorRuntimeCompilation();


            #region EF有关
            services.AddDbContext<DataBaseContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("DbContext"),
                    i =>
                    {
                        i.EnableRetryOnFailure();//可自定义失败重连次数
                        i.CommandTimeout(60);
                        i.UseRowNumberForPaging();
                    });
            });

            services.AddScoped<IDataBaseService, DataBaseService>();
            #endregion


            #region 支持跨域  所有的Api都支持跨域
            services.AddCors(option => option.AddPolicy("AllowCors", _build => _build.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader()));
            #endregion

            #region 缓存
            services.AddSession();
            services.AddResponseCaching();
            services.AddSingleton<CookieHelper>();
            services.AddSingleton<SessionHelper>();
            #endregion



            //使用HttpContext获取器扩展 用来获取静态HttpContext
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();


            #region JWT验证
            //services.Configure<JWTTokenOptions>(Configuration.GetSection("audience"));
            //1.Nuget引入程序包：Microsoft.AspNetCore.Authentication.JwtBearer 
            //var validAudience = this.Configuration["audience"];
            //var validIssuer = this.Configuration["issuer"];
            //var securityKey = this.Configuration["SecurityKey"];
            //services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)  //默认授权机制名称；                                      
            //.AddJwtBearer(options =>
            //{
            //    options.TokenValidationParameters = new TokenValidationParameters
            //    {
            //        ValidateIssuer = true,//是否验证Issuer
            //        ValidateAudience = true,//是否验证Audience
            //        ValidateLifetime = true,//是否验证失效时间
            //        ValidateIssuerSigningKey = true,//是否验证SecurityKey
            //        ValidAudience = validAudience,//Audience
            //        ValidIssuer = validIssuer,//Issuer，这两项和前面签发jwt的设置一致  表示谁签发的Token
            //        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(securityKey))//拿到SecurityKey
            //    };
            //});
            #endregion


        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            //脚本启动，这里需要改
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot"))
            });

            #region 缓存
            app.UseSession();
            app.UseResponseCaching();
            #endregion


            #region 支持跨域
            app.UseCors("AllowCors");
            #endregion



            app.UseRouting();

            #region 通过中间件来支持鉴权授权
            //app.UseAuthentication();
            //app.UseAuthorization();
            #endregion



            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                  name: "default",
                  pattern: "{controller=Login}/{action=Login2}/{id?}");

                //匹配路由区域
                endpoints.MapAreaControllerRoute(
                  name: "AceArea", "Ace",
                  pattern: "{area:exists}/{controller=Home}/{action=Login}/{id?}");
                //匹配路由区域

                endpoints.MapAreaControllerRoute(
                  name: "LayUIArea", "LayUI",
                  pattern: "{area:exists}/{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}

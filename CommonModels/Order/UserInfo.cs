﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagementSystem.Models.Order
{
    public class UserInfo
    {
        public int AccountId { get; set; }
        public string Account { get; set; }
        public string MobilePhone { get; set; }
        public string EMail { get; set; }
        public string UserName { get; set; }
    }
}

﻿using System;

namespace ManagementSystem.Models
{
    /// <summary>
    /// 用户表实体类
    /// </summary>
    public class SysUser
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}

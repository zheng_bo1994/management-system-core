﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ManagementSystem.Models.Sys
{
    public class Sys_User : Sys_Model
    {
        /// <summary>
        /// 关联账号ID
        /// </summary>
        public int AccountId { get; set; }
        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 用户图片
        /// </summary>
        public string UserImage { get; set; }
        /// <summary>
        /// 用户代号
        /// </summary>
        public string UserCode { get; set; }

        [NotMapped]
        public List<Sys_Role> RoleList { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace ManagementSystem.Models.Sys
{
    public class Sys_Entity
    {
        [Key]
        public int Id { get; set; }
    }
}

﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ManagementSystem.Models.Sys
{
    public class Sys_Permission : Sys_Entity
    {
        /// <summary>
        /// 权限名称
        /// </summary>
        public string PermissionName { get; set; }


        /// <summary>
        /// 权限描述
        /// </summary>
        public string Description { get; set; }

        [NotMapped]
        public List<Sys_Page> PageList { get; set; }
    }
}

﻿namespace ManagementSystem.Models.Sys
{
    public class Sys_Account : Sys_Entity
    {
        /// <summary>
        /// 账户
        /// </summary>
        public string Account { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// 手机
        /// </summary>
        public string MobilePhone { get; set; }
        /// <summary>
        /// 电子邮件
        /// </summary>
        public string EMail { get; set; }
    }
}

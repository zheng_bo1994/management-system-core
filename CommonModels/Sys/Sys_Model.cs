﻿using System;

namespace ManagementSystem.Models.Sys
{
    public class Sys_Model : Sys_Entity
    {
        public DateTime CreationTime { get; set; }
        public int CreationBy { get; set; }
    }
}

﻿namespace ManagementSystem.Models.Sys
{
    public class Sys_PermissionRelPage : Sys_Entity
    {
        public int PermissionId { get; set; }
        public int PageId { get; set; }
    }
}

﻿namespace ManagementSystem.Models.Sys
{
    public class Sys_AccountRelRole : Sys_Entity
    {
        public int AccountId { get; set; }
        public int RoleId { get; set; }
    }
}

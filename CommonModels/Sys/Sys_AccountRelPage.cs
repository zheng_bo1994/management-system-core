﻿namespace ManagementSystem.Models.Sys
{
    public class Sys_AccountRelPage : Sys_Entity
    {
        public int AccountId { get; set; }
        public int PageId { get; set; }
    }
}

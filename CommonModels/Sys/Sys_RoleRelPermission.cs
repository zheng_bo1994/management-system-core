﻿namespace ManagementSystem.Models.Sys
{
    public class Sys_RoleRelPermission : Sys_Entity
    {
        public int RoleId { get; set; }
        public int PermissionId { get; set; }
    }
}

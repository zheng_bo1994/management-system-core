﻿using ManagementSystem.Models.Sys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagementSystem.Models.Results
{
    public class RoleInfoListForJqgridResult : Sys_Entity
    {
        public string RoleName { get; set; }
        public string Description { get; set; }
        public string RoleType { get; set; }
    }
}

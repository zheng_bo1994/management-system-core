﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagementSystem.Models.Results
{
    public class JqGridData
    {
        public int page { get; set; }//当前页码（1开始）
        public int total { get; set; }//总页数
        public int records { get; set; }//总记录数（不分页的记录数总和）
        public IList rows { get; set; }//当前页数据


        public static JqGridData ConvertIListToJqGridData(int currentPage, int pageSize, int totalRecordCount, IList currentPageData)
        {
            JqGridData result = new JqGridData();
            result.page = currentPage;
            result.records = totalRecordCount;
            result.rows = currentPageData;
            result.total = (int)Math.Ceiling((double)totalRecordCount / pageSize);
            return result;
        }
    }


}

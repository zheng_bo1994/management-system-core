﻿using ManagementSystem.Models.Sys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagementSystem.Models.Results
{
    public class PermissionInfoListForJqgridResult : Sys_Entity
    {
        /// <summary>
        /// 权限名称
        /// </summary>
        public string PermissionName { get; set; }


        /// <summary>
        /// 权限描述
        /// </summary>
        public string Description { get; set; }
    }
}

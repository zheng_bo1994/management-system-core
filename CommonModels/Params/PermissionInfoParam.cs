﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagementSystem.Models.Params
{
    public class PermissionInfoParam : JqgridParam
    {
        public string PermissionName { get; set; }
        public string Description { get; set; }
    }
}

﻿using ManagementSystem.Models.Sys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagementSystem.Models.Params
{
    public class JqgridParam : Sys_Entity
    {
        public int rows { get; set; }
        public int page { get; set; }
    }
}

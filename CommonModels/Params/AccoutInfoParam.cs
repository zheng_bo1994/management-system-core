﻿using ManagementSystem.Models.Params;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagementSystem.Models.Params
{
    public class AccoutInfoParam: JqgridParam
    {
        public string Account { get; set; }
        public string Password { get; set; }
        public string OnePassword { get; set; }
        public string MobilePhone { get; set; }
        public string EMail { get; set; }
    }
}

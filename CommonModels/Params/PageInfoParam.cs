﻿using ManagementSystem.Models.Params;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagementSystem.Models.Params
{
    public class PageInfoParam : JqgridParam
    {
        public string PageName { get; set; }
        public string Url { get; set; }
        public string Icon { get; set; }
        public int ParentId { get; set; }
        public int Sequence { get; set; }
        public int Level { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagementSystem.Models.Params
{
    public class MenuInfo
    {
        /// <summary>
        /// 菜单级别
        /// </summary>
        public int MenuLevel { get; set; }
        /// <summary>
        /// 菜单名称
        /// </summary>
        public int MenuName { get; set; }
        /// <summary>
        /// 父菜单名称
        /// </summary>
        public int FatherMenuName { get; set; }
        /// <summary>
        /// 父菜单编号
        /// </summary>
        public int FatherMenuId { get; set; }
        /// <summary>
        /// 是否有子菜单
        /// </summary>
        public bool IsSub { get; set; }
    }
}

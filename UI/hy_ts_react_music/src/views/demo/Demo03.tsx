import React, { PureComponent, ReactNode } from 'react'

interface IProps {
  name: string
  age?: number
}

interface IState {
  message: string
  counter: number
}

// interface IWHY {
//   aaa: string
//   address: string
// }

class Demo3 extends PureComponent<IProps, IState> {
  state = {
    message: '',
    counter: 100
  }

  constructor(props: IProps) {
    super(props)
  }

  increment() {
    this.setState({ counter: this.state.counter + 1 })
  }

  render(): ReactNode {
    return (
      <div>
        name: {this.props.name}
        age: {this.props.age}
        message: {this.state.message}
        counter: {this.state.counter}
        <button onClick={() => this.increment()}>+1</button>
      </div>
    )
  }
}

export default Demo3

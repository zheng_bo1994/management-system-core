import React, { memo, useRef, useState } from 'react'
import type { FC, ReactNode, ElementRef } from 'react'
import { useAppSelector } from '@/store'
import { BannerControl, BannerLeft, BannerRight, BannerWrapper } from './style'
import { Carousel } from 'antd'
import { shallowEqual } from 'react-redux'
import classNames from 'classnames'

interface IProps {
  children?: ReactNode
}

const TopBanner: FC<IProps> = () => {
  /** 定义组件内部的数据 */
  const [currentIndex, setCurrentIndex] = useState(0)
  const bannerRef = useRef<ElementRef<typeof Carousel>>(null)
  const divRef = useRef<HTMLDivElement>(null)

  /** redux中获取数据 */
  const { banners } = useAppSelector(
    (state) => ({
      banners: state.recommend.banners
    }),
    shallowEqual
  )

  /** 事件监听的方法 */
  function handleBeforeChange(from: number, to: number) {
    setCurrentIndex(-1)
    setTimeout(() => {
      setCurrentIndex(to)
    }, 1000)
  }
  function handleAfterChange(current: number) {
    // setCurrentIndex(current)
  }

  const bgImageUrl = useRef<string>()
  if (currentIndex >= 0 && banners.length > 0) {
    bgImageUrl.current =
      banners[currentIndex].imageUrl + '?imageView&blur=40x20'
    console.log(currentIndex, bgImageUrl.current)
  }

  return (
    <BannerWrapper bgImage={bgImageUrl.current}>
      <div className="banner wrap-v2" ref={divRef}>
        <BannerLeft>
          <div className="banner-list">
            {banners.map((item: any) => {
              return (
                <div className="banner-item" key={item.imageUrl}>
                  <img
                    className="image"
                    src={item.imageUrl}
                    alt={item.typeTitle}
                  />
                </div>
              )
            })}
          </div>
          {/* <Carousel
            dots={false}
            autoplay
            effect="fade"
            ref={bannerRef}
            beforeChange={handleBeforeChange}
            afterChange={handleAfterChange}
            autoplaySpeed={5000}
            speed={5000}
          >
            {banners.map((item: any) => {
              return (
                <div className="banner-item" key={item.imageUrl}>
                  <img
                    className="image"
                    src={item.imageUrl}
                    alt={item.typeTitle}
                  />
                </div>
              )
            })}
          </Carousel> */}
          <ul className="dots">
            {banners.map((item, index) => {
              return (
                <li key={item.imageUrl}>
                  <span
                    className={classNames('item', {
                      active: currentIndex === index
                    })}
                  ></span>
                </li>
              )
            })}
          </ul>
        </BannerLeft>
        <BannerRight></BannerRight>
        <BannerControl>
          <button
            className="btn left"
            onClick={() => bannerRef.current?.prev()}
          ></button>
          <button
            className="btn right"
            onClick={() => bannerRef.current?.next()}
          ></button>
        </BannerControl>
      </div>
    </BannerWrapper>
  )
}

export default memo(TopBanner)

﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ManagementSystem.DBContext.Interface
{
    public interface ISimpleClientT<T> where T : class, new()
    {
        SqlSugarClient CreateSqlSugarClient(string connstr);
        ISugarQueryable<T> AsQueryable();

        IInsertable<T> AsInsertable(T insertObj);

        IInsertable<T> AsInsertable(T[] insertObjs);

        IInsertable<T> AsInsertable(List<T> insertObjs);

        IUpdateable<T> AsUpdateable(T updateObj);

        IUpdateable<T> AsUpdateable(T[] updateObjs);

        IUpdateable<T> AsUpdateable(List<T> updateObjs);

        IDeleteable<T> AsDeleteable();


        T GetById(dynamic id);

        List<T> GetList();


        List<T> GetList(Expression<Func<T, bool>> whereExpression);

        T GetSingle(Expression<Func<T, bool>> whereExpression);

        List<T> GetPageList(Expression<Func<T, bool>> whereExpression, PageModel page);

        List<T> GetPageList(Expression<Func<T, bool>> whereExpression, PageModel page, Expression<Func<T, object>> orderByExpression = null, OrderByType orderByType = OrderByType.Asc);

        List<T> GetPageList(List<IConditionalModel> conditionalList, PageModel page);

        List<T> GetPageList(List<IConditionalModel> conditionalList, PageModel page, Expression<Func<T, object>> orderByExpression = null, OrderByType orderByType = OrderByType.Asc);

        bool IsAny(Expression<Func<T, bool>> whereExpression);

        int Count(Expression<Func<T, bool>> whereExpression);


        bool Insert(T insertObj);

        int InsertReturnIdentity(T insertObj);

        bool InsertRange(T[] insertObjs);

        bool InsertRange(List<T> insertObjs);

        bool Update(T updateObj);

        bool UpdateRange(T[] updateObjs);

        bool UpdateRange(List<T> updateObjs);

        bool Update(Expression<Func<T, object>> columns, Expression<Func<T, bool>> whereExpression);

        bool Delete(T deleteObj);

        bool Delete(Expression<Func<T, bool>> whereExpression);

        bool DeleteById(dynamic id);

        bool DeleteByIds(dynamic[] ids);

    }
}

﻿using ManagementSystem.DBContext.Interface;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ManagementSystem.DBContext.Service
{
    public partial class SimpleClientT<T> : ISimpleClientT<T> where T : class, new()
    {
        #region 构造函数


        protected SqlSugarClient Context { get; set; }
        public SqlSugarClient FullClient { get { return Context; } }

        private SimpleClientT()
        {

        }
        public SimpleClientT(SqlSugarClient context)
        {
            Context = context;
        }


        #endregion

        /// <summary>
        /// 连接数据库
        /// </summary>
        /// <param name="connstr"></param>
        public SqlSugarClient CreateSqlSugarClient(string connstr)
        {
            SqlSugarClient sqlSugarClient = new SqlSugarClient(
                new ConnectionConfig()
                {
                    ConnectionString = connstr,
                    DbType = DbType.SqlServer, //设置数据库类型
                    IsAutoCloseConnection = true, //自动释放数据务，如果存在事务，在事务结束后释放
                    InitKeyType = InitKeyType.Attribute, //从实体特性中读取主键自增列信息
                });

            return sqlSugarClient;
        }

        public ISugarQueryable<T> AsQueryable()
        {
            return Context.Queryable<T>();
        }


        public IInsertable<T> AsInsertable(T insertObj)
        {
            return Context.Insertable<T>(insertObj);
        }
        public IInsertable<T> AsInsertable(T[] insertObjs)
        {
            return Context.Insertable<T>(insertObjs);
        }
        public IInsertable<T> AsInsertable(List<T> insertObjs)
        {
            return Context.Insertable<T>(insertObjs);
        }
        public IUpdateable<T> AsUpdateable(T updateObj)
        {
            return Context.Updateable<T>(updateObj);
        }
        public IUpdateable<T> AsUpdateable(T[] updateObjs)
        {
            return Context.Updateable<T>(updateObjs);
        }
        public IUpdateable<T> AsUpdateable(List<T> updateObjs)
        {
            return Context.Updateable<T>(updateObjs);
        }
        public IDeleteable<T> AsDeleteable()
        {
            return Context.Deleteable<T>();
        }

        public T GetById(dynamic id)
        {
            return Context.Queryable<T>().InSingle(id);
        }
        public List<T> GetList()
        {
            return Context.Queryable<T>().ToList();
        }

        public List<T> GetList(Expression<Func<T, bool>> whereExpression)
        {
            return Context.Queryable<T>().Where(whereExpression).ToList();
        }
        public T GetSingle(Expression<Func<T, bool>> whereExpression)
        {
            return Context.Queryable<T>().Single(whereExpression);
        }
        public List<T> GetPageList(Expression<Func<T, bool>> whereExpression, PageModel page)
        {
            int count = 0;
            var result = Context.Queryable<T>().Where(whereExpression).ToPageList(page.PageIndex, page.PageSize, ref count);
            page.TotalCount = count;
            return result;
        }
        public List<T> GetPageList(Expression<Func<T, bool>> whereExpression, PageModel page, Expression<Func<T, object>> orderByExpression = null, OrderByType orderByType = OrderByType.Asc)
        {
            int count = 0;
            var result = Context.Queryable<T>().OrderByIF(orderByExpression != null, orderByExpression, orderByType).Where(whereExpression).ToPageList(page.PageIndex, page.PageSize, ref count);
            page.TotalCount = count;
            return result;
        }
        public List<T> GetPageList(List<IConditionalModel> conditionalList, PageModel page)
        {
            int count = 0;
            var result = Context.Queryable<T>().Where(conditionalList).ToPageList(page.PageIndex, page.PageSize, ref count);
            page.TotalCount = count;
            return result;
        }
        public List<T> GetPageList(List<IConditionalModel> conditionalList, PageModel page, Expression<Func<T, object>> orderByExpression = null, OrderByType orderByType = OrderByType.Asc)
        {
            int count = 0;
            var result = Context.Queryable<T>().OrderByIF(orderByExpression != null, orderByExpression, orderByType).Where(conditionalList).ToPageList(page.PageIndex, page.PageSize, ref count);
            page.TotalCount = count;
            return result;
        }
        public bool IsAny(Expression<Func<T, bool>> whereExpression)
        {
            return Context.Queryable<T>().Where(whereExpression).Any();
        }
        public int Count(Expression<Func<T, bool>> whereExpression)
        {

            return Context.Queryable<T>().Where(whereExpression).Count();
        }

        public bool Insert(T insertObj)
        {
            return Context.Insertable(insertObj).ExecuteCommand() > 0;
        }
        public int InsertReturnIdentity(T insertObj)
        {
            return Context.Insertable(insertObj).ExecuteReturnIdentity();
        }
        public bool InsertRange(T[] insertObjs)
        {
            return Context.Insertable(insertObjs).ExecuteCommand() > 0;
        }
        public bool InsertRange(List<T> insertObjs)
        {
            return Context.Insertable(insertObjs).ExecuteCommand() > 0;
        }
        public bool Update(T updateObj)
        {
            return Context.Updateable(updateObj).ExecuteCommand() > 0;
        }
        public bool UpdateRange(T[] updateObjs)
        {
            return Context.Updateable(updateObjs).ExecuteCommand() > 0;
        }
        public bool UpdateRange(List<T> updateObjs)
        {
            return Context.Updateable(updateObjs).ExecuteCommand() > 0;
        }
        public bool Update(Expression<Func<T, object>> columns, Expression<Func<T, bool>> whereExpression)
        {
            return Context.Updateable<T>().UpdateColumns(columns).Where(whereExpression).ExecuteCommand() > 0;
        }
        public bool Delete(T deleteObj)
        {
            return Context.Deleteable<T>().Where(deleteObj).ExecuteCommand() > 0;
        }
        public bool Delete(Expression<Func<T, bool>> whereExpression)
        {
            return Context.Deleteable<T>().Where(whereExpression).ExecuteCommand() > 0;
        }
        public bool DeleteById(dynamic id)
        {
            return Context.Deleteable<T>().In(id).ExecuteCommand() > 0;
        }
        public bool DeleteByIds(dynamic[] ids)
        {
            return Context.Deleteable<T>().In(ids).ExecuteCommand() > 0;
        }
    }
}

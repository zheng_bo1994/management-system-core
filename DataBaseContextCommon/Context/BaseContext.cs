﻿using ManagementSystem.Models;
using ManagementSystem.Models.Sys;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagementSystem.DBContext
{
    /// <summary>
    /// EF操作上下文类
    /// </summary>
    public class BaseContext : DbContext
    {
        public BaseContext(DbContextOptions<BaseContext> options) : base(options)
        {

        }

        #region 实体集
        //public DbSet<Sys_Account> Sys_Account { get; set; } 
        public DbSet<Sys_Permission> Sys_Permission { get; set; } //注意 这里名字和实体名字必须一致
        //public DbSet<Sys_Page> Sys_Page { get; set; } 
        #endregion
    }
}

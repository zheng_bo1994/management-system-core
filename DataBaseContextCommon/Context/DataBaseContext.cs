﻿using ManagementSystem.Models.Sys;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagementSystem.DBContext.Context
{
    public class DataBaseContext : DbContext
    {
        #region 依赖注入
        public DataBaseContext(DbContextOptions<DataBaseContext> options) : base(options)
        {

        }



        #endregion Identity

        #region  添加表
        #region  系统有关
        public virtual DbSet<Sys_Account> Sys_Account { get; set; }
        public virtual DbSet<Sys_Page> Sys_Page { get; set; }
        public virtual DbSet<Sys_Permission> Sys_Permission { get; set; }
        public virtual DbSet<Sys_Role> Sys_Role { get; set; }
        public virtual DbSet<Sys_User> Sys_User { get; set; }
        public virtual DbSet<Sys_PermissionRelPage> Sys_PermissionRelPage { get; set; }
        public virtual DbSet<Sys_RoleRelPermission> Sys_RoleRelPermission { get; set; }
        public virtual DbSet<Sys_AccountRelPage> Sys_AccountRelPage { get; set; }
        public virtual DbSet<Sys_AccountRelRole> Sys_AccountRelRole { get; set; }
        #endregion

        #region  自定义表
        //public virtual DbSet<Biz_ChargeManagement> Biz_ChargeManagement { get; set; }
        //public virtual DbSet<Biz_Parking> Biz_Parking { get; set; }
        //public virtual DbSet<Biz_ParkingLotManagement> Biz_ParkingLotManagement { get; set; }
        #endregion
        #endregion


    }
}
